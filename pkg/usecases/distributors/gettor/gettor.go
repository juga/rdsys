// Copyright (c) 2021-2022, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package gettor

import (
	"bufio"
	"io"
	"log"
	"strings"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/internal"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/core"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/delivery"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/delivery/mechanisms"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/usecases/resources"
)

const (
	DistName = "gettor"

	CommandHelp  = "help"
	CommandLinks = "links"
)

var (
	requestsCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "gettor_request_total",
		Help: "The total number of gettor requests",
	},
		[]string{"command", "platform", "locale"},
	)

	linkResponseCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "gettor_link_response_total",
		Help: "The total number of gettor link responses",
	},
		[]string{"platform", "locale"},
	)
)

var platformAliases = map[string]string{
	"linux":   "linux64",
	"lin":     "linux64",
	"windows": "win32",
	"win":     "win32",
	"osx":     "osx64",
	"macos":   "osx64",
	"mac":     "osx64",
}

type GettorDistributor struct {
	ipc      delivery.Mechanism
	wg       sync.WaitGroup
	shutdown chan bool
	tblinks  TBLinkList

	// latest version of Tor Browser per platform
	version map[string]resources.Version

	// locales map a lowercase locale to its correctly cased locale
	locales map[string]string

	mutex sync.RWMutex
}

// TBLinkList are indexed first by platform and last by locale
type TBLinkList map[string]map[string][]*resources.TBLink

type Command struct {
	Locale   string
	Platform string
	Command  string
}

func (d *GettorDistributor) GetLinks(platform, locale string) []*resources.TBLink {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	linkResponseCount.WithLabelValues(platform, locale).Inc()
	return d.tblinks[platform][locale]
}

func (d *GettorDistributor) ParseCommand(body io.Reader) *Command {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	command := Command{
		Locale:   "",
		Platform: "",
		Command:  "",
	}

	scanner := bufio.NewScanner(body)
	requestedPlatform := ""
	for scanner.Scan() {
		if command.Locale != "" && command.Platform != "" {
			break
		}

		line := strings.ToLower(strings.TrimSpace(scanner.Text()))
		if len(line) == 0 || line[0] == '>' || (len(line) >= 3 && line[0:3] == "re:") {
			continue
		}

		for _, word := range strings.Fields(line) {
			if command.Locale == "" {
				locale, exists := d.locales[word]
				if exists {
					command.Locale = locale
					continue
				}
			}

			if command.Platform == "" {
				platform, exists := platformAliases[word]
				if exists {
					requestedPlatform = word
					command.Platform = platform
					continue
				}

				_, exists = d.tblinks[word]
				if exists {
					requestedPlatform = word
					command.Platform = word
					continue
				}
			}
		}
	}
	requestsCount.WithLabelValues(command.Command, requestedPlatform, command.Locale).Inc()

	if command.Platform != "" {
		command.Command = CommandLinks
	} else {
		command.Command = CommandHelp
	}

	if command.Locale == "" {
		if len(d.locales) != 0 {
			command.Locale = "en-US"
		} else {
			command.Locale = "ALL"
		}
	}

	return &command
}

func (d *GettorDistributor) SupportedPlatforms() []string {
	d.mutex.RLock()
	defer d.mutex.RUnlock()

	platforms := make([]string, 0, len(platformAliases)+len(d.tblinks))
	for platform := range platformAliases {
		platforms = append(platforms, platform)
	}
	for platform := range d.tblinks {
		platforms = append(platforms, platform)
	}
	return platforms
}

// housekeeping listens to updates from the backend resources
func (d *GettorDistributor) housekeeping(rStream chan *core.ResourceDiff) {
	defer d.wg.Done()
	defer close(rStream)
	defer d.ipc.StopStream()

	for {
		select {
		case diff := <-rStream:
			d.applyDiff(diff)
		case <-d.shutdown:
			log.Printf("Shutting down housekeeping.")
			return
		}
	}
}

func (d *GettorDistributor) Init(cfg *internal.Config) {
	d.shutdown = make(chan bool)
	d.tblinks = make(TBLinkList)
	d.locales = make(map[string]string)
	d.version = make(map[string]resources.Version)

	d.ipc = mechanisms.NewHttpsIpc(
		cfg.Backend.ResourceStreamURL(),
		"GET",
		cfg.Backend.ApiTokens[DistName])
	rStream := make(chan *core.ResourceDiff)
	req := core.ResourceRequest{
		RequestOrigin: DistName,
		ResourceTypes: cfg.Distributors.Gettor.Resources,
		Receiver:      rStream,
	}
	d.ipc.StartStream(&req)

	d.wg.Add(1)
	go d.housekeeping(rStream)
}

func (d *GettorDistributor) Shutdown() {
	close(d.shutdown)
	d.wg.Wait()
}

// applyDiff to tblinks. Ignore changes, links should not change, just appear new or be gone
func (d *GettorDistributor) applyDiff(diff *core.ResourceDiff) {
	d.mutex.Lock()
	defer d.mutex.Unlock()

	if diff.FullUpdate {
		d.tblinks = make(TBLinkList)
		d.locales = make(map[string]string)
		d.version = make(map[string]resources.Version)
	}

	needsCleanUp := map[string]struct{}{}
	for rType, resourceQueue := range diff.New {
		if rType != "tblink" {
			continue
		}
	processResource:
		for _, r := range resourceQueue {
			link, ok := r.(*resources.TBLink)
			if !ok {
				log.Println("Not valid tblink resource", r)
				continue
			}
			version, ok := d.version[link.Platform]
			if ok {
				switch version.Compare(link.Version) {
				case 1:
					// ignore resources with old versions
					continue
				case -1:
					d.version[link.Platform] = link.Version
					needsCleanUp[link.Platform] = struct{}{}
				}
			} else {
				d.version[link.Platform] = link.Version
			}

			_, ok = d.tblinks[link.Platform]
			if !ok {
				d.tblinks[link.Platform] = make(map[string][]*resources.TBLink)
			}
			for _, l := range d.tblinks[link.Platform][link.Locale] {
				if l.Uid() == link.Uid() {
					continue processResource
				}
			}
			d.tblinks[link.Platform][link.Locale] = append(d.tblinks[link.Platform][link.Locale], link)

			if link.Locale != "ALL" {
				d.locales[strings.ToLower(link.Locale)] = link.Locale
				if strings.Contains(link.Locale, "-") {
					for _, part := range strings.Split(link.Locale, "-") {
						lang := strings.ToLower(part)
						if _, ok := d.locales[lang]; !ok {
							d.locales[lang] = link.Locale
						}
					}
				}
			} else {
				d.locales = make(map[string]string)
			}
		}
	}

	for rType, resourceQueue := range diff.Gone {
		if rType != "tblink" {
			continue
		}
		for _, r := range resourceQueue {
			link, ok := r.(*resources.TBLink)
			if !ok {
				log.Println("Not valid tblink resource", r)
				continue
			}
			_, ok = d.tblinks[link.Platform]
			if !ok {
				continue
			}
			for i, l := range d.tblinks[link.Platform][link.Locale] {
				if l.Link == link.Link {
					linklist := d.tblinks[link.Platform][link.Locale]
					d.tblinks[link.Platform][link.Locale] = append(linklist[:i], linklist[i+1:]...)
					break
				}
			}
		}
	}

	for platform := range needsCleanUp {
		d.deleteOldVersions(platform)
	}
}

// deleteOldVersions assumes that the mutex is already locked
func (d *GettorDistributor) deleteOldVersions(platform string) {
	locales := d.tblinks[platform]
	for locale, res := range locales {
		newResources := []*resources.TBLink{}
		for _, r := range res {
			if d.version[platform].Compare(r.Version) == 0 {
				newResources = append(newResources, r)
			}
		}

		if len(newResources) == 0 {
			delete(d.tblinks[platform], locale)
		} else {
			d.tblinks[platform][locale] = newResources
		}
	}
	if len(d.tblinks[platform]) == 0 {
		delete(d.tblinks, platform)
	}
}
