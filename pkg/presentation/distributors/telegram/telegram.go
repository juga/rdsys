// Copyright (c) 2021-2022, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package telegram

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/internal"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/persistence"
	pjson "gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/persistence/json"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/usecases/distributors/telegram"
	tb "gopkg.in/telebot.v3"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

const (
	TelegramPollTimeout = 10 * time.Second
)

type TBot struct {
	bot          *tb.Bot
	dist         *telegram.TelegramDistributor
	updateTokens map[string]string
}

// InitFrontend is the entry point to telegram'ss frontend.  It connects to telegram over
// the bot API and waits for user commands.
func InitFrontend(cfg *internal.Config) {
	newBridgesStore := make(map[string]persistence.Mechanism, len(cfg.Distributors.Telegram.UpdaterTokens))
	for updater := range cfg.Distributors.Telegram.UpdaterTokens {
		newBridgesStore[updater] = pjson.New(updater, cfg.Distributors.Telegram.StorageDir)
	}

	dist := telegram.TelegramDistributor{
		NewBridgesStore: newBridgesStore,
	}
	dist.Init(cfg)

	tbot, err := newTBot(cfg.Distributors.Telegram.Token, &dist)
	if err != nil {
		log.Fatal(err)
	}
	tbot.updateTokens = cfg.Distributors.Telegram.UpdaterTokens

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT)
	signal.Notify(signalChan, syscall.SIGTERM)
	go func() {
		<-signalChan
		log.Printf("Caught SIGINT.")
		dist.Shutdown()

		log.Printf("Shutting down the telegram bot.")
		tbot.Stop()
	}()

	http.HandleFunc("/update", tbot.updateHandler)
	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(cfg.Distributors.Telegram.ApiAddress, nil)

	tbot.Start()
}

func newTBot(token string, dist *telegram.TelegramDistributor) (*TBot, error) {
	var t TBot
	var err error

	t.dist = dist
	t.bot, err = tb.NewBot(tb.Settings{
		Token:  token,
		Poller: &tb.LongPoller{Timeout: TelegramPollTimeout},
	})
	if err != nil {
		return nil, err
	}

	t.bot.Handle("/start", t.getMenu)
	t.bot.Handle("/bridges", t.getBridges)
	return &t, nil
}

func (t *TBot) Start() {
	t.bot.Start()
}

func (t *TBot) Stop() {
	t.bot.Stop()
}

func (t *TBot) getMenu(c tb.Context) error {

	var (
		menu       = &tb.ReplyMarkup{ResizeKeyboard: true}
		btnBridges = menu.Text("Bridges")
		btnHelp    = menu.Text("Help")
	)

	menu.Reply(
		menu.Row(btnBridges),
		menu.Row(btnHelp),
	)

	t.bot.Send(c.Sender(), "Welcome! To get bridges, type /bridges "+
		"or press the Bridges button. \n\n"+
		"To get information about how to use your bridges, "+
		"type /help or press the Help button.",
		menu)

	helpmsg := "To use your bridges on Android:\n\n" +
		"1. When you start Tor Browser, " +
		"click the Settings icon.\n\n" +
		"2. Select 'Config Bridge'.\n\n" +
		"3. Make sure the 'Use a Bridge' setting is " +
		"switched on and that the 'obfs4' option " +
		"is selected.\n\n" +
		"4. Copy the message with the bridges " +
		"you received.\n\n" +
		"5. Select 'Provide a Bridge I know' and " +
		"paste the bridges into the pop-up.\n\n" +
		"6. Return to the connect page and " +
		"press the 'Connect' button.\n\n\n" +
		"To use your bridges on desktop:\n\n" +
		"1. In the menu with three bars (≡) in " +
		"the upper right corner, select " +
		"'Settings'. In the left column, " +
		"select 'Connection'. " +
		"If you launched Tor Browser without " +
		"connecting, you can also press the " +
		"'Configure Connection...' button.\n\n" +
		"2. Under the 'Bridges' section, switch on the " +
		"'Your Current Bridges' setting.\n\n" +
		"3. Copy the message with the bridges " +
		"you received.\n\n" +
		"4. Under 'Add a New Bridge', click the " +
		"'Add a Bridge Manually...' button.\n\n" +
		"5. Paste the bridges into the 'Provide a " +
		"Bridge' pop-up (one bridge per line).\n\n" +
		"6. If Tor Browser is already connected to Tor, " +
		"restart it to save your changes. " +
		"If Tor Browser is not connected to Tor, select " +
		"'Connect' at the top of the connection page."

	t.bot.Handle("/help", func(c tb.Context) error {
		return c.Send(helpmsg, menu)
	})

	t.bot.Handle(&btnHelp, func(c tb.Context) error {
		return c.Send(helpmsg, menu)
	})

	t.bot.Handle(&btnBridges, t.getBridges)

	return nil
}

func (t *TBot) getBridges(c tb.Context) error {
	if c.Sender().IsBot {
		return c.Send("No bridges for bots, sorry")
	}
	userID := c.Sender().ID
	resources := t.dist.GetResources(userID)
	t.bot.Send(c.Sender(), "***Your bridges:***", tb.ModeMarkdown)
	response := ""
	for _, r := range resources {
		response += "\n" + r.String()
	}
	t.bot.Send(c.Sender(), response, tb.ModeMarkdown)

	return nil
}

func (t *TBot) updateHandler(w http.ResponseWriter, r *http.Request) {
	name := t.getTokenName(w, r)
	if name == "" {
		return
	}
	defer r.Body.Close()

	err := t.dist.LoadNewBridges(name, r.Body)
	if err != nil {
		log.Printf("Error loading bridges: %v", err)
		http.Error(w, "error while loading bridges", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (t *TBot) getTokenName(w http.ResponseWriter, r *http.Request) string {
	tokenLine := r.Header.Get("Authorization")
	if tokenLine == "" {
		log.Printf("Request carries no 'Authorization' HTTP header.")
		http.Error(w, "request carries no 'Authorization' HTTP header", http.StatusBadRequest)
		return ""
	}
	if !strings.HasPrefix(tokenLine, "Bearer ") {
		log.Printf("Authorization header contains no bearer token.")
		http.Error(w, "authorization header contains no bearer token", http.StatusBadRequest)
		return ""
	}
	fields := strings.Split(tokenLine, " ")
	givenToken := fields[1]

	for name, savedToken := range t.updateTokens {
		if givenToken == savedToken {
			return name
		}
	}

	log.Printf("Invalid authentication token.")
	http.Error(w, "invalid authentication token", http.StatusUnauthorized)
	return ""
}
