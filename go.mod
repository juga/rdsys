module gitlab.torproject.org/tpo/anti-censorship/rdsys

go 1.14

require (
	github.com/NullHypothesis/zoossh v0.0.0-20211012143359-017a7be2e713
	github.com/aws/aws-sdk-go-v2 v1.10.0
	github.com/aws/aws-sdk-go-v2/service/s3 v1.17.0
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac // indirect
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/prometheus/client_golang v1.11.1
	github.com/prometheus/common v0.31.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/stretchr/testify v1.8.0
	github.com/xanzy/go-gitlab v0.50.3
	gitlab.torproject.org/tpo/anti-censorship/geoip v0.0.0-20210928150955-7ce4b3d98d01
	golang.org/x/oauth2 v0.0.0-20220411215720-9780585627b5
	google.golang.org/api v0.81.0
	gopkg.in/telebot.v3 v3.1.2
)
